﻿
namespace ExporterWebApi.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum SearchResultErrors
    {
        /// <summary>
        /// The accessKey is not valid
        /// </summary>
        AccessDenied = 0,
        /// <summary>
        /// The API threw up, and the Exporter business logic cannot parse it
        /// </summary>
        ResultParseError = 1,
        /// <summary>
        /// An error occurred within the DAM Center API while attempting to execute the search
        /// </summary>
        ApiError = 2
    }
}
