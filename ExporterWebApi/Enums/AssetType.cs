﻿namespace ExporterWebApi.Enums
{
    /// <summary>
    /// The different asset types
    /// </summary>
    public enum AssetType
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        Video = 1,
        Audio = 2,
        Image = 4,
        PowerPoint = 5,
        Html = 6,
        Text = 7,
        Word = 8,
        Excel = 9,
        InDesign = 10,
        Zip = 11,
        Meta = 12,
        Pdf = 14,
        Archive = 15,
        Photoshop = 16,
        Illustrator = 17,
        Odt = 100,
        Ott = 101,
        Ods = 102,
        Ots = 103,
        Odp = 105,
        Otp = 106,
        Odg = 107,
        Otg = 108,
        Odb = 109,
        Odf = 110,
        Odm = 111,
        Oth = 112,
        Live = 1000
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}
