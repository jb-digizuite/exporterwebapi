﻿namespace ExporterWebApi.Enums
{
    /// <summary>
    /// The different possibilities when transforming meta data
    /// </summary>
    public enum MetadataTransformType
    {
        /// <summary>
        /// All assets' meta data is transformed to a single Excel document
        /// </summary>
        Excel = 1,

        /// <summary>
        /// The meta data is transformed to one XML document per exported asset
        /// </summary>
        XmlFiles = 2
    }
}
