﻿namespace ExporterWebApi.Enums
{
    /// <summary>
    /// The different possibilities when exporting assets
    /// </summary>
    public enum AssetExportType
    {
        /// <summary>
        /// Exports assets to the local disc
        /// </summary>
        LocalFolder = 1,

        /// <summary>
        /// Exports asset to disc, and then sends a mail with a download URL
        /// </summary>
        ToMail = 2
    }
}
