﻿namespace ExporterWebApi.Enums
{
    /// <summary>
    /// The different possibilities when exporting meta data
    /// </summary>
    public enum MetadataDeliverType
    {
        /// <summary>
        /// Exports meta data as XML documents to local disc.
        /// </summary>
        XmlToLocalFolder = 1,

        /// <summary>
        /// Exports meta data as an Excel document to local disc.
        /// </summary>
        ExcelToLocalFolder = 2,

        /// <summary>
        /// Exports the meta data to disc, and then sends a mail with a download URL
        /// </summary>
        ToMail = 3
    }
}
