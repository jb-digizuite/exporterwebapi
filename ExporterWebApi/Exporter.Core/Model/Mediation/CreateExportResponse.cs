﻿using Newtonsoft.Json;

namespace ExporterWebApi.Exporter.Core.Model.Mediation
{
    /// <summary>
    /// The response model used when the client wants the export immediately
    /// </summary>
    public class CreateExportResponse : BaseResponse
    {
        /// <summary>
        /// An URL, which can be used to download a specific export
        /// </summary>
        public string DownloadUrl { get; set; }

        /// <summary>
        /// Used to get the folder, the export was placed in.
        /// </summary>
        [JsonIgnore]
        public string FolderPath { get; set; }
    }
}
