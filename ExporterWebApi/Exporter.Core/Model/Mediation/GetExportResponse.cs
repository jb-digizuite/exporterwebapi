﻿using Newtonsoft.Json;

namespace ExporterWebApi.Exporter.Core.Model.Mediation
{
    //Cannot be bothered witing comments for this response
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public class GetExportResponse : BaseResponse
    {
        [JsonIgnore]
        public string UniqueFolderName { get; set; }
        [JsonIgnore]
        public bool DeleteExport { get; set; }
    }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}
