﻿namespace ExporterWebApi.Exporter.Core.Model.Mediation
{
    /// <summary>
    /// Defines the different types of error codes, which the Exporter API may return to the client
    /// </summary>
    public enum ErrorCode
    {
        /// <summary>
        /// No errors have occurred
        /// </summary>
        NoError = 0,

        /// <summary>
        /// The request's accessKey is invalid
        /// </summary>
        NotValid = 1,

        /// <summary>
        /// The search returned no assets, i.e. there's nothing to export. Or a request to download an export couldn't find the requested export 
        /// </summary>
        NotFound = 2,

        /// <summary>
        /// An unknown error has occurred
        /// </summary>
        Unknown = 3,

        /// <summary>
        /// An error occurred while search for assets. 
        /// </summary>
        SearchError = 4
    }
}
