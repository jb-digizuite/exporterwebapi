﻿namespace ExporterWebApi.Exporter.Core.Model.Mediation
{
    /// <summary>
    /// The response that's typically returned to the client
    /// </summary>
    public class BaseResponse
    {
        /// <summary>
        /// Tell the client the type of error that's occurred
        /// </summary>
        public ErrorCode ErrorCode { get; set; }

        /// <summary>
        /// An explanation of any errors, that may have occurred
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Tells the client whether an error has occurred
        /// </summary>
        public bool HasError => ErrorCode != ErrorCode.NoError;
    }
}
