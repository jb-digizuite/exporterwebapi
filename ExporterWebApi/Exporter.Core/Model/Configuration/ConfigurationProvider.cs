﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ExporterWebApi.Enums;
using ExporterWebApi.Exporter.WebApi.Controllers.RequestModel;
using ExporterWebApi.Exporter.WebApi.Controllers.RequestModel.Create;
using ExporterWebApi.Interfaces;
using ExporterWebApi.MessageHost.Messages.ExportRequest;
using Microsoft.Extensions.Configuration;

namespace ExporterWebApi.Exporter.Core.Model.Configuration
{
    /// <summary>
    /// Class responsible for parsing the appsettings and overwriting some of these appsettings with the settings supplied by each export request
    /// </summary>
    public class ConfigurationProvider : IProvideConfiguration
    {
        /// <summary>
        /// The URL of the DAM Center API
        /// </summary>
        public Uri DamApiUrl { get; }
        
        /// <summary>
        /// The search used to extract meta data and download information for assets
        /// </summary>
        public string SearchName { get; }
        
        /// <summary>
        /// The different possibilities when transforming meta data
        /// </summary>
        public MetadataTransformType MetadataTransformType { get; private set; }

        /// <summary>
        /// The different possibilities when exporting assets
        /// </summary>
        public AssetExportType AssetExportType { get; set; }

        /// <summary>
        /// The different possibilities when exporting meta data
        /// </summary>
        public MetadataDeliverType MetadataDeliverType { get; set; }

        /// <summary>
        /// Determines whether assets are exported
        /// </summary>
        public bool ExportAssets { get; set; }

        /// <summary>
        /// Determines whether meta data is exported
        /// </summary>
        public bool ExportMetadata { get; set; }

        /// <summary>
        /// The path to the folder where the exports are placed on the local disc
        /// </summary>
        public string FolderPath { get; set; }

        /// <summary>
        /// Determines whether the exported assets should be "soft deleted"
        /// </summary>
        public bool DeleteExportedAssets { get; private set; }

        /// <summary>
        /// The Search2 search XML ID of the Title valueField. Becomes a part of an asset's file name
        /// </summary>
        public string TitleFieldId { get; }

        /// <summary>
        /// The Search2 search XML ID of the extension valueField. Becomes an asset's file extension
        /// </summary>
        public string ExtensionFieldId { get; }

        /// <summary>
        /// The Search2 search XML ID of the URL used to download an asset's physical file
        /// </summary>
        public string UrlFieldId { get; }

        /// <summary>
        /// The Search2 search XML ID of the total amount of assets, which can be returned by the search. 
        /// </summary>
        public string TotalFieldId { get; }

        /// <summary>
        /// The Search2 search XML ID of the asset type
        /// </summary>
        public string AssetTypeFieldId { get; }

        /// <inheritdoc />
        public int MaxDegreeOfParallelism { get; }

        /// <summary>
        /// Search2 search XML valueField IDs to exclude from the extracted meta data
        /// </summary>
        public List<string> MetadataExludeFieldIds { get; private set; }

        /// <summary>
        /// Some exported meta data are not simple types, i.e. "multi combo value". These types are joined into a string separated by this char
        /// </summary>
        public string Splitter { get; }

        /// <summary>
        /// A search parameter. If this is supplied, only assets within the specified channel will be exported
        /// </summary>
        public int ChannelId { get; set; }

        /// <summary>
        /// A search parameter. If this is supplied, only assets within the specified catalog folder will be exported
        /// </summary>
        public int CatalogFolderId { get; set; }

        /// <summary>
        /// A search parameter. If this is supplied, only assets with the specified asset IDs will be exported
        /// </summary>
        public List<int> AssetIds { get; set; }

        /// <summary>
        /// Part of the search paging information. Determines the maximum of assets returned by each search request
        /// </summary>
        public int AssetsPerPage { get; }

        /// <summary>
        /// The mail address used to inform that an export has completed and is ready for download
        /// </summary>
        public string MailAdress { get; set; }

        /// <summary>
        /// Authenticates a user when the API retrieves a request. Also used to authorize the read access to the assets and meta data when executing the search
        /// </summary>
        public string AccessKey { get; set; }

        /// <summary>
        /// Used to create the DownloadUrl
        /// </summary>
        public Uri ExporterApiBaseUrl { get; set; }

        /// <summary>
        /// Used to tell the clients where the export can be downloaded
        /// </summary>
        public string DownloadExportUrl { get; set; }

        /// <inheritdoc />
        public Guid UniqueFolderName { get; set; }

        /// <summary>
        /// Reads all the configurations from the appsettings
        /// </summary>
        /// <param name="configuration">Responsible for getting the configuration from appsettings</param>
        public ConfigurationProvider(IConfiguration configuration)
        {
            var url = configuration.GetSection("AppConfiguration")["damApiUrl"];
            DamApiUrl = new Uri(url);

            url = configuration.GetSection("AppConfiguration")["exporterApiBaseUrl"].TrimEnd('/');
            ExporterApiBaseUrl = new Uri(url);

            SearchName = configuration.GetSection("AppConfiguration")["searchName"];
            FolderPath = configuration.GetSection("AppConfiguration")["folderPath"];
            TitleFieldId = configuration.GetSection("AppConfiguration")["titleFieldId"];
            ExtensionFieldId = configuration.GetSection("AppConfiguration")["extensionFieldId"];
            UrlFieldId = configuration.GetSection("AppConfiguration")["urlFieldId"];
            TotalFieldId = configuration.GetSection("AppConfiguration")["totalFieldId"];
            AssetTypeFieldId = configuration.GetSection("AppConfiguration")["assetTypeFieldId"];

            MaxDegreeOfParallelism = int.TryParse(configuration.GetSection("AppConfiguration")["maxDegreeOfParallelism"], out var maxDegreeOfParallelism) ? maxDegreeOfParallelism : 4;

            MetadataExludeFieldIds = configuration.GetSection("AppConfiguration")["metadataExcludeFieldIds"].Split(',').ToList();

            Splitter = configuration.GetSection("AppConfiguration")["splitter"];

            if (int.TryParse(configuration.GetSection("AppConfiguration")["assetsPerPage"], out var assetsPerPage))
                AssetsPerPage = assetsPerPage;
            else
                throw new ArgumentException($"Could not parse the value of assetsPerPage from AppConfiguration. The value = \"{configuration.GetSection("AppConfiguration")["assetsPerPage"]}\"");
        }

        /// <summary>
        /// Overwrites some configurations with settings, the client has decided upon
        /// </summary>
        /// <param name="request">An NServicebus message</param>
        public void OverwriteConfigs(IExportRequest request)
        {
            if (request.MetadataExludeFieldIds != null)
                MetadataExludeFieldIds.AddRange(request.MetadataExludeFieldIds);

            DeleteExportedAssets = request.DeleteExportedAssets;
            ExportAssets = request.ExportAssets;
            ExportMetadata = request.ExportMetadata;

            MailAdress = request.MailAddress;

            AccessKey = request.AccessKey;

            SetFolderNameAndDownloadUrl(request.AccessKey);

            MetadataTransformType = Enum.Parse<MetadataTransformType>(request.MetadataTransformType);

            //The IExportRequest comes from an NServiceBus queue, i.e. send the export via mail.
            AssetExportType = AssetExportType.ToMail;
            MetadataDeliverType = MetadataDeliverType.ToMail;

            if (request.AssetIds != null && request.AssetIds.Count > 0)
                AssetIds = request.AssetIds;
            else if (request.CatalogFolderId > 0)
                CatalogFolderId = request.CatalogFolderId;
            else if (request.ChannelId > 0)
                ChannelId = request.ChannelId;
        }

        /// <summary>
        /// Overwrites some configurations with settings, the client has decided upon
        /// </summary>
        /// <param name="request">The base HTTP request</param>
        public void OverwriteConfigs(BaseRequest request)
        {
            if (request.MetadataExludeFieldIds != null)
                MetadataExludeFieldIds.AddRange(request.MetadataExludeFieldIds);

            DeleteExportedAssets = request.DeleteExportedAssets ?? false;
            ExportAssets = request.ExportAssets ?? true;
            ExportMetadata = request.ExportMetadata ?? true;

            AccessKey = request.AccessKey;

            SetFolderNameAndDownloadUrl(request.AccessKey);

            MetadataTransformType = request.MetadataTransformType ?? MetadataTransformType.XmlFiles;
            AssetExportType = AssetExportType.LocalFolder;

            switch (request.MetadataTransformType)
            {
                case MetadataTransformType.Excel:
                    MetadataDeliverType = MetadataDeliverType.ExcelToLocalFolder;
                    break;
                case MetadataTransformType.XmlFiles:
                    MetadataDeliverType = MetadataDeliverType.XmlToLocalFolder;
                    break;
            }

            switch (request)
            {
                case CreateAssetIdsExportRequest assetIdsExportRequest:
                    AssetIds = assetIdsExportRequest.AssetIds;
                    break;
                case CreateCatalogFolderIdExportRequest catalogFolderIdExportRequest:
                    CatalogFolderId = catalogFolderIdExportRequest.CatalogFolderId;
                    break;
                case CreateChannelIdExportRequest downloadChannelIdExportRequest:
                    ChannelId = downloadChannelIdExportRequest.ChannelId;
                    break;
            }
        }

        private void SetFolderNameAndDownloadUrl(string accessKey)
        {
            UniqueFolderName = Guid.NewGuid();
            FolderPath = Path.Combine(FolderPath, UniqueFolderName.ToString());

            DownloadExportUrl = $"{ExporterApiBaseUrl}/{UniqueFolderName}?accessKey={accessKey}&deleteExport=true";
        }
    }
}