﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using ExporterWebApi.Enums;
using ExporterWebApi.Interfaces;
using Newtonsoft.Json.Linq;

namespace ExporterWebApi.Exporter.Core.Model
{
    internal class Asset : IAsset
    {
        private int _itemId;
        private Uri _url;

        private AssetType _assetType;

        public Dictionary<string, string> Metadata { get; set; }

        public int ItemId => _itemId;

        public string FileName { get; set; }

        public Asset(JObject item, IProvideConfiguration configs)
        {
            ParseMetadata(item, configs);
        }

        private void ParseMetadata(JObject item, IProvideConfiguration configs)
        {
            var title = item.SelectToken($"$.{configs.TitleFieldId}").ToString();
            var extension = item.SelectToken($"$.{configs.ExtensionFieldId}").ToString();
            _itemId = int.Parse(item.SelectToken("$.itemId").ToString());

            var assetType = item.SelectToken($"$.{configs.AssetTypeFieldId}").ToString();
            if (!Enum.TryParse(assetType, out _assetType))
                throw new ArgumentException($"Could not parse the value of \"assetTypeFieldId\" to a valid member of {nameof(AssetType)}");

            var url = item.SelectToken($"$.{configs.UrlFieldId}").ToString();
            if (configs.ExportAssets && _assetType != AssetType.Meta)
                _url = new Uri(url);

            var exludedFieldIds = configs.MetadataExludeFieldIds;

            Metadata = new Dictionary<string, string>();
            foreach (KeyValuePair<string, JToken> token in item)
            {
                if (exludedFieldIds.Contains(token.Key))
                    continue;

                var value = "";
                if (token.Value.Type != JTokenType.Array)
                    value = token.Value.Value<string>();
                else
                {
                    foreach (JToken child in token.Value.Children())
                        value += child.Value<string>() + configs.Splitter;

                    value = value.TrimEnd(configs.Splitter.ToCharArray());
                }

                //Minor hack to support the importer.
                var key = token.Key;
                if (key.Equals("itemId"))
                    key = "ItemId";

                Metadata.Add(key, value);
            }

            if (title == default(string) && _assetType != AssetType.Meta && (extension == default(string) || _itemId == default(int)))
                throw new ArgumentException(
                    $"Could not find values for either {nameof(configs.TitleFieldId)} (found value = null), {nameof(configs.ExtensionFieldId)} (found value = {extension}) for asset with item ID {ItemId}. Without all of these, assets and meta data cannot be exported!");

            var name = $"{_itemId}{title}";
            if (!string.IsNullOrWhiteSpace(extension))
            {
                name += $".{extension}";
            }
            FileName = StripIllegalFileNameChars(name);
        }

        public void DownloadAsssetToFolder(string folderPath)
        {
            if (_assetType == AssetType.Meta)
                return;

            var path = Path.Combine(folderPath, FileName);
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(_url, path);
            }
        }

        private string StripIllegalFileNameChars(string fileName)
        {
            var illegalChars = Path.GetInvalidFileNameChars();
            var regexCondition = "[";
            foreach (char illegalChar in illegalChars)
            {
                regexCondition += illegalChar;
            }
            regexCondition += "]";

            return Regex.Replace(fileName, regexCondition, string.Empty);
        }
    }
}
