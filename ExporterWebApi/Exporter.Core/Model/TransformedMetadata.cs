﻿using ExporterWebApi.Interfaces;

namespace ExporterWebApi.Exporter.Core.Model
{
    internal class TransformedMetadata : ITransformedMetadata
    {
        public object Metadata { get; }
        public string Filename { get; }

        public TransformedMetadata(object metadata, string filename)
        {
            Metadata = metadata;
            Filename = filename;
        }
    }
}
