﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Mail;
using ExporterWebApi.Exporter.Core.Model.Mediation;
using ExporterWebApi.Exporter.WebApi;
using ExporterWebApi.Interfaces;
using Microsoft.Extensions.Configuration;

namespace ExporterWebApi.Exporter.Core
{
    /// <summary>
    /// Zips directories and sends mails
    /// </summary>
    public class MailAndZipFolderSender : ISendMailAndZipFolder
    {
        private readonly string _storageFolderPath, _uncDomain, _uncPassword, _uncUsername;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appSettings">Only used to get the "uncFolderPathInStorage" and UNC credentials from appSettings</param>
        public MailAndZipFolderSender(IConfiguration appSettings)
        {
            _storageFolderPath = appSettings.GetSection("AppConfiguration")["uncFolderPathInStorage"];
            _uncDomain = appSettings.GetSection("AppConfiguration")["uncDomain"];
            _uncPassword = appSettings.GetSection("AppConfiguration")["uncPassword"];
            _uncUsername = appSettings.GetSection("AppConfiguration")["uncUser"];
        }

        /// <summary>
        /// Sends a mail with the zipped file as an attachment
        /// </summary>
        /// <param name="mailAddress">The mail address of the recipient</param>
        /// <param name="downloadUrl">The URL used to download the export</param>
        /// <param name="folderPath"></param>
        /// <param name="errorMessage">Any errors to include in the mail</param>
        public void SendMail(MailAddress mailAddress, string downloadUrl, string folderPath, string errorMessage)
        {
            ZipAndMoveExportToStorage(folderPath);

            //TODO replace with message to NotificationService and proper subject and body
            using (var message = new MailMessage
            {
                From = new MailAddress("jeppe@bullshit.com"),
                To = { mailAddress },
                Subject = "this is a subject",
                Body = $"this is the body. And here's the download URL: {downloadUrl}",
            })
            using (var mailClient = new SmtpClient
            {
                Host = "smtp.sendgrid.net",
                Port = 587,
                Credentials = new NetworkCredential("apikey",
                    "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")
            })
            {
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    message.Body += $"\nAn unexpected error occurred: {errorMessage}.\nThe DAM Center could only export a subset of the selected assets.";
                }
                mailClient.Send(message);
                message.Attachments.Dispose();
            }
        }

        /// <inheritdoc />
        public void ZipAndMoveExportToStorage(string folderPath)
        {
            var zippedFilePath = CreateZipAndReturnPath(folderPath);

            var uniqueFolderName = new DirectoryInfo(folderPath).Name;
            var to = Path.Combine(_storageFolderPath, uniqueFolderName + @".zip");

            using (var uncAccessor = new UncAccessWithCredentials(null))
            {
                if (uncAccessor.NetUseWithCredentials(_storageFolderPath, _uncUsername, _uncDomain, _uncPassword))
                {
                    File.Copy(zippedFilePath, to);
                }
                else
                    throw new Exception("Could not connect to the UNC storage folder. Please verify the unc appsettings.");
            }
        }

        /// <inheritdoc />
        public byte[] GetZippedExportFromStorage(string uniqueFileName, bool deleteExport)
        {
            byte[] bytes;
            using (var uncAccessor = new UncAccessWithCredentials(null))
            {
                if (uncAccessor.NetUseWithCredentials(_storageFolderPath, _uncUsername, _uncDomain, _uncPassword))
                {
                    var zipPath = GetZipPathOnStorage(uniqueFileName);
                    bytes = File.ReadAllBytes(zipPath);
                    if (deleteExport)
                    {
                        File.Delete(zipPath);
                    }
                }
                else
                    throw new Exception("Could not connect to the UNC storage folder. Please verify the unc appsettings.");
            }

            return bytes;
        }

        private string CreateZipAndReturnPath(string folderPath)
        {
            var uniqueFolderName = new DirectoryInfo(folderPath).Name;
            var zipPath = GetZipPathOnLocalhost(folderPath, uniqueFolderName);

            if (File.Exists(zipPath))
            {
                return zipPath;
            }

            var zipDirectory = Path.GetDirectoryName(zipPath);
            if (!Directory.Exists(zipDirectory))
                Directory.CreateDirectory(zipDirectory);

            ZipFile.CreateFromDirectory(folderPath, zipPath, CompressionLevel.Optimal, false);

            return zipPath;
        }

        private string GetZipPathOnLocalhost(string folderPath, string uniqueFolderName)
        {
            return Path.Combine(folderPath, $@"..\zippedExports{uniqueFolderName}\Exports.zip");
        }

        private string GetZipPathOnStorage(string uniqueFolderName)
        {
            return Path.Combine(_storageFolderPath, uniqueFolderName + ".zip");
        }
    }
}
