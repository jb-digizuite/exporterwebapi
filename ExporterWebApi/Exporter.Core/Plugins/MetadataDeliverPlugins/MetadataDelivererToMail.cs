﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using ExporterWebApi.Enums;
using ExporterWebApi.Interfaces;

namespace ExporterWebApi.Exporter.Core.Plugins.MetadataDeliverPlugins
{
    /// <summary>
    /// Exports assets to mail
    /// </summary>
    public class MetadataDelivererToMail : IDeliverMetadata
    {
        private readonly ISendMailAndZipFolder _mailAndZipFolderSender;
        private readonly XmlMetadataDeliverToLocalFolder _xmlMetadataDelivererToDisc;
        private readonly ExcelMetadataDeliverToLocalFolder _excelMetadataDelivererToDisc;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mailAndZipFolderSender"></param>
        public MetadataDelivererToMail(ISendMailAndZipFolder mailAndZipFolderSender)
        {
            _mailAndZipFolderSender = mailAndZipFolderSender;
            //TODO replace "new" with dependency injection. If it's necessary???
            _xmlMetadataDelivererToDisc = new XmlMetadataDeliverToLocalFolder();
            _excelMetadataDelivererToDisc = new ExcelMetadataDeliverToLocalFolder();
        }

        /// <summary>
        /// Delivers meta data
        /// </summary>
        /// <param name="metadata">The meta data to deliver</param>
        /// <param name="configs">Determines where the meta data is delivered to</param>
        /// <param name="sendExports">True if the supplied meta data is the last to be delivered</param>
        /// <param name="errorMesage">Any eventual error message. Used when delivering the export to somewhere other then the client.</param>
        public void DeliverMetadata(List<ITransformedMetadata> metadata, IProvideConfiguration configs, bool sendExports, string errorMesage)
        {
            DeliverMetadataToDisc(metadata, configs, sendExports);

            if (sendExports)
                _mailAndZipFolderSender.SendMail(new MailAddress(configs.MailAdress), configs.DownloadExportUrl, configs.FolderPath, errorMesage);            
        }

        private void DeliverMetadataToDisc(List<ITransformedMetadata> metadata, IProvideConfiguration configs, bool sendExports)
        {
            switch (configs.MetadataTransformType)
            {
                case MetadataTransformType.XmlFiles:
                    _xmlMetadataDelivererToDisc.DeliverMetadata(metadata, configs);
                    break;
                case MetadataTransformType.Excel:
                    _excelMetadataDelivererToDisc.DeliverMetadata(metadata, configs, sendExports, "");
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"Got an unexpected value of {nameof(MetadataTransformType)}. Accepted values are \"XmlFiles\" and \"Excel\".");
            }
        }
    }
}
