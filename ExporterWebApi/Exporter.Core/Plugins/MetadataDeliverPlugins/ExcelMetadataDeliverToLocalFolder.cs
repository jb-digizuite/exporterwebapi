﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ExporterWebApi.Interfaces;

namespace ExporterWebApi.Exporter.Core.Plugins.MetadataDeliverPlugins
{
    internal class ExcelMetadataDeliverToLocalFolder : IDeliverMetadata
    {
        private const string Filename = "assets.xlsx";
        private DataTable _table;

        /// <summary>
        /// Delivers meta data in the form of an Excel document
        /// </summary>
        /// <param name="metadata">The meta data to deliver</param>
        /// <param name="configs">Determines where the meta data is delivered to</param>
        /// <param name="sendExports">True if the supplied meta data is the last to be delivered</param>
        /// <param name="errorMesage">Any eventual error message. Not used by this class.</param>
        public void DeliverMetadata(List<ITransformedMetadata> metadata, IProvideConfiguration configs, bool sendExports, string errorMesage)
        {
            var newTable = (DataTable) metadata[0].Metadata;
            if (_table == null)
                _table = newTable;
            else
                _table.Merge(newTable);

            if (!sendExports && string.IsNullOrWhiteSpace(errorMesage))
                return;

            var fullPath = Path.Combine(configs.FolderPath, Filename);
            SaveNewExcelDoc(_table, fullPath);
        }

        private void SaveNewExcelDoc(DataTable table, string fullPath)
        {
            //Grabbed this approach from https://stackoverflow.com/questions/23041021/how-to-write-some-data-to-excel-file-xlsx
            using (var workbook = new XLWorkbook())
            {
                using (var sheet = workbook.Worksheets.Add("assets"))
                {
                    sheet.Cell(1, 1).InsertTable(table);

                    sheet.Columns().AdjustToContents();

                    if (File.Exists(fullPath))
                    {
                        File.Delete(fullPath);
                    }

                    var dir = Path.GetDirectoryName(fullPath);
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }

                    workbook.SaveAs(fullPath);
                }
            }
        }
    }
}



//var fullPath = Path.Combine(configs.ZippedFilePath, Filename);

//var newTable = (DataTable) metadata[0].Metadata;
//var existingTable = GeTableFromExisintExcelDoc(fullPath);

//if (existingTable != null)
//{
//    existingTable.Merge(newTable);
//    SaveNewExcelDoc(existingTable, fullPath);
//}
//else
//    SaveNewExcelDoc(newTable, fullPath);

//private DataTable GeTableFromExisintExcelDoc(string fullPath)
//{
//    if (!File.Exists(fullPath))
//    {
//        return null;
//    }
//    //Open the Excel file using ClosedXML.
//    using (XLWorkbook workBook = new XLWorkbook(fullPath, XLEventTracking.Disabled))
//    {
//        //Read the first Sheet from Excel file.
//        IXLWorksheet workSheet = workBook.Worksheet(1);

//        //Create a new DataTable.
//        DataTable dt = new DataTable();

//        //Loop through the Worksheet rows.
//        bool firstRow = true;
//        foreach (IXLRow row in workSheet.Rows())
//        {
//            //Use the first row to add columns to DataTable.
//            if (firstRow)
//            {
//                foreach (IXLCell cell in row.Cells())
//                {
//                    dt.Columns.Add(cell.Value.ToString());
//                }
//                firstRow = false;
//            }
//            else
//            {
//                //Add rows to DataTable.
//                dt.Rows.Add();
//                int i = 0;
//                foreach (IXLCell cell in row.Cells())
//                {
//                    dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
//                    i++;
//                }
//            }
//        }

//        return dt;
//    }
//}