﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using ExporterWebApi.Interfaces;

namespace ExporterWebApi.Exporter.Core.Plugins.MetadataDeliverPlugins
{
    internal class XmlMetadataDeliverToLocalFolder : IDeliverMetadata
    {
        /// <summary>
        /// Delivers meta data
        /// </summary>
        /// <param name="metadata">The meta data to deliver</param>
        /// <param name="configs">Determines where the meta data is delivered to</param>
        /// <param name="sendExports">True if the supplied meta data is the last to be delivered</param>
        /// <param name="errorMesage">Any eventual error message. Used when delivering the export to somewhere other then the client.</param>
        public void DeliverMetadata(List<ITransformedMetadata> metadata, IProvideConfiguration configs, bool sendExports = false, string errorMesage = "")
        {
            var folderPath = configs.FolderPath;
            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            foreach (var transformedMetadata in metadata)
            {
                var path = Path.Combine(folderPath, transformedMetadata.Filename);

                var xml = (XElement) transformedMetadata.Metadata;

                xml.Save(path);
            }
        }
    }
}
