﻿using System.Collections.Generic;
using System.Net.Mail;
using ExporterWebApi.Interfaces;
using log4net;

namespace ExporterWebApi.Exporter.Core.Plugins.AssetExportPlugins
{
    /// <summary>
    /// Exports assets to a mail address
    /// </summary>
    public class AssetExporterToMail : IDeliverAssets
    {
        private readonly ISendMailAndZipFolder _exportsZipperAndSender;
        private readonly IDeliverAssets _assetsToFolderDeliverer;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exportsZipperAndSender"></param>
        public AssetExporterToMail(ISendMailAndZipFolder exportsZipperAndSender)
        {
            _exportsZipperAndSender = exportsZipperAndSender;
            //TODO replace with dependency injection
            _assetsToFolderDeliverer = new AssetExporterToFolder(LogManager.GetLogger(typeof(AssetExporterToFolder)));
        }

        /// <summary>
        /// Delivers assets to local disc. Sends a mail when all assets have been exported to disc
        /// </summary>
        /// <param name="assets"></param>
        /// <param name="configs"></param>
        /// <param name="sendExports"></param>
        /// <param name="errorMesage"></param>
        public void DeliverAssets(List<IAsset> assets, IProvideConfiguration configs, bool sendExports, string errorMesage)
        {
            _assetsToFolderDeliverer.DeliverAssets(assets, configs, false, null);
            if (!configs.ExportMetadata && sendExports)
                _exportsZipperAndSender.SendMail(new MailAddress(configs.MailAdress), configs.DownloadExportUrl, configs.FolderPath, errorMesage);
        }
    }
}
