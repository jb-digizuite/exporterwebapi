﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using ExporterWebApi.Interfaces;
using log4net;

namespace ExporterWebApi.Exporter.Core.Plugins.AssetExportPlugins
{
    internal class AssetExporterToFolder : IDeliverAssets
    {
        private readonly ILog _logger;
        private string _folderPath;

        public AssetExporterToFolder(ILog logger)
        {
            _logger = logger;
        }


        public void DeliverAssets(List<IAsset> assets, IProvideConfiguration configs, bool sendExports = false, string errorMesage = "")
        {
            var stopwatch = Stopwatch.StartNew();

            if (!Directory.Exists(configs.FolderPath))
                Directory.CreateDirectory(configs.FolderPath);

            _folderPath = configs.FolderPath;

            Parallel.ForEach(assets, new ParallelOptions { MaxDegreeOfParallelism = configs.MaxDegreeOfParallelism }, DownloadAndSaveFileToLocalFolder);

            stopwatch.Stop();
            _logger.Fatal($"time taken to download {assets.Count} assets to disc = {stopwatch.ElapsedMilliseconds}");
        }

        private void DownloadAndSaveFileToLocalFolder(IAsset asset)
        {
            _logger.Debug($"Downloading {asset.FileName} to {_folderPath}");

            asset.DownloadAsssetToFolder(_folderPath);
        }
    }
}
