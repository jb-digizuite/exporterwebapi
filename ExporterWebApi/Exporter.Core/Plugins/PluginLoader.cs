﻿using Autofac;
using ExporterWebApi.Enums;
using ExporterWebApi.Exporter.Core.Plugins.AssetExportPlugins;
using ExporterWebApi.Exporter.Core.Plugins.MetadataDeliverPlugins;
using ExporterWebApi.Exporter.Core.Plugins.MetadataTransformPlugins;
using ExporterWebApi.Exporter.WebApi.Controllers.RequestModel;
using ExporterWebApi.Interfaces;
using ExporterWebApi.MessageHost.Messages.ExportRequest;
using log4net;
using IContainer = Autofac.IContainer;

namespace ExporterWebApi.Exporter.Core.Plugins
{
    internal class PluginLoader : ILoadPlugins
    {
        private readonly IContainer _container;
        private readonly IDeleteAssets _deleter;
        private readonly ISearchForAssetsAndMetadata _searcher;

        /// <summary>
        /// Determines what plugins should be used
        /// </summary>
        public IProvideConfiguration Configs { get; set; }

        public PluginLoader(IProvideConfiguration configs, ISearchForAssetsAndMetadata searcher, IDeleteAssets deleter, ISendMailAndZipFolder exportsZipperAndSender)
        {
            _deleter = deleter;
            _searcher = searcher;

            Configs = configs;


            var builder = new ContainerBuilder();

            //Asset exporters
            builder.RegisterType<AssetExporterToFolder>().Keyed<IDeliverAssets>(AssetExportType.LocalFolder);
            builder.RegisterType<AssetExporterToMail>().Keyed<IDeliverAssets>(AssetExportType.ToMail);

            //metadata transformers
            builder.RegisterType<MetadataTransformerToExcelFile>().Keyed<ITransformMetadata>(MetadataTransformType.Excel);
            builder.RegisterType<MetadataTransformerToXml>().Keyed<ITransformMetadata>(MetadataTransformType.XmlFiles);

            //metadata exporters
            builder.RegisterType<ExcelMetadataDeliverToLocalFolder>().Keyed<IDeliverMetadata>(MetadataDeliverType.ExcelToLocalFolder);
            builder.RegisterType<XmlMetadataDeliverToLocalFolder>().Keyed<IDeliverMetadata>(MetadataDeliverType.XmlToLocalFolder);
            builder.RegisterType<MetadataDelivererToMail>().Keyed<IDeliverMetadata>(MetadataDeliverType.ToMail);

            builder.RegisterInstance(exportsZipperAndSender);

            builder.Register(logger =>
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType));

            _container = builder.Build();
        }

        /// <summary>
        /// Gets an exporter instance with the plugins specified by the Configs propery.
        /// </summary>
        /// <returns></returns>
        public IExport GetExporter(BaseRequest request)
        {
            Configs.OverwriteConfigs(request);
            return GetExporter();
        }

        public IExport GetExporter(ExportRequest request)
        {
            Configs.OverwriteConfigs(request);
            return GetExporter();
        }

        private IExport GetExporter()
        {
            var assetDeliverer = _container.ResolveKeyed<IDeliverAssets>(Configs.AssetExportType);
            var metadataTransformer = _container.ResolveKeyed<ITransformMetadata>(Configs.MetadataTransformType);
            var metadataDeliverer = _container.ResolveKeyed<IDeliverMetadata>(Configs.MetadataDeliverType);

            _searcher.Configs = Configs;

            return new Exporter(Configs, _searcher, _deleter, assetDeliverer, metadataTransformer, metadataDeliverer, LogManager.GetLogger(typeof(Exporter)));
        }
    }
}

//private readonly List<ITransformMetadata> metadataTransformPlugins;
//private readonly List<IDeliverAssets> assetExportPlugins;
//private readonly List<IDeliverMetadata> metadataDeliverPlugins;

//public PluginLoader()
//{
//    metadataTransformPlugins = new List<ITransformMetadata>
//    {
//        new MetadataTransformerToXml(),
//        new MetadataTransformerToExcelFile()
//    };

//    assetExportPlugins = new List<IDeliverAssets>
//    {
//        new AssetExporterToFolder()
//    };

//    metadataDeliverPlugins = new List<IDeliverMetadata>
//    {
//        new XmlMetadataDeliverToLocalFolder(),
//        new ExcelMetadataDeliverToLocalFolder()
//    };


//}

//public IDeliverAssets GetAssetDeliverer(AssetExportType pluginType)
//{
//    foreach (IDeliverAssets plugin in assetExportPlugins)
//    {
//        if (plugin.Type != pluginType)
//            continue;

//        return plugin;
//    }

//    throw new ArgumentException($"Cannot find a {nameof(IDeliverAssets)} plugin, which matches the given {nameof(AssetExportType)} of {pluginType}");
//}

//public ITransformMetadata GetMetadataTransformer(MetadataTransformType pluginType)
//{
//    foreach (ITransformMetadata plugin in metadataTransformPlugins)
//    {
//        if (plugin.Type != pluginType)
//            continue;

//        return plugin;
//    }

//    throw new ArgumentException($"Cannot find a {nameof(ITransformMetadata)} plugin, which matches the given {nameof(MetadataTransformType)} of {pluginType}");
//}

//public IDeliverMetadata GetMetadataDeliverer(MetadataDeliverType pluginType)
//{
//    foreach (IDeliverMetadata plugin in metadataDeliverPlugins)
//    {
//        if (plugin.Type != pluginType)
//            continue;

//        return plugin;
//    }

//    throw new ArgumentException($"Cannot find a {nameof(IDeliverMetadata)} plugin, which matches the given {nameof(MetadataDeliverType)} of {pluginType}");
//}