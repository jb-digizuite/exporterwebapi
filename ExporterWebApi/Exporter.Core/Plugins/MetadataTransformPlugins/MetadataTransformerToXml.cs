﻿using System.Collections.Generic;
using System.Xml.Linq;
using ExporterWebApi.Exporter.Core.Model;
using ExporterWebApi.Interfaces;

namespace ExporterWebApi.Exporter.Core.Plugins.MetadataTransformPlugins
{
    internal class MetadataTransformerToXml : ITransformMetadata
    {
        /// <summary>
        /// Transforms the meta data into XML documents
        /// </summary>
        /// <param name="assets">The objects containing the meta data to transform</param>
        /// <param name="configs">May contain info used to transform the meta data</param>
        /// <returns></returns>
        public List<ITransformedMetadata> TransformMetadata(List<IAsset> assets, IProvideConfiguration configs)
        {
            var xmlDocs = new List<ITransformedMetadata>();
            foreach (IAsset asset in assets)
            {
                var xml = new XElement("Root");
                foreach (KeyValuePair<string, string> metadata in asset.Metadata)
                {
                    var values = metadata.Value.Split(configs.Splitter.ToCharArray());
                    foreach (string value in values)
                    {
                        var child = new XElement(metadata.Key) {Value = value};
                        xml.AddFirst(child);
                    }
                }

                xmlDocs.Add(new TransformedMetadata(xml, asset.FileName + ".xml"));
            }

            return xmlDocs;
        }
    }
}
