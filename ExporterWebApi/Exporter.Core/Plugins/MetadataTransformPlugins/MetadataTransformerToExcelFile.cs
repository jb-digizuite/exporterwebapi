﻿using System.Collections.Generic;
using System.Data;
using ExporterWebApi.Exporter.Core.Model;
using ExporterWebApi.Interfaces;

namespace ExporterWebApi.Exporter.Core.Plugins.MetadataTransformPlugins
{
    internal class MetadataTransformerToExcelFile : ITransformMetadata
    {
        /// <summary>
        /// Transforms the meta data into an Excel document
        /// </summary>
        /// <param name="assets">The objects containing the meta data to transform</param>
        /// <param name="configs">May contain info used to transform the meta data</param>
        /// <returns></returns>
        public List<ITransformedMetadata> TransformMetadata(List<IAsset> assets, IProvideConfiguration configs)
        {
            var table = GetDataTable(assets);
            return new List<ITransformedMetadata>
            {
                new TransformedMetadata(table, "")
            };
        }

        private DataTable GetDataTable(List<IAsset> assets)
        {
            var allColumnNames = new List<string>();
            foreach (IAsset asset in assets)
                foreach (KeyValuePair<string, string> metadata in asset.Metadata)
                    if (!allColumnNames.Contains(metadata.Key))
                        allColumnNames.Add(metadata.Key);


            var table = new DataTable("assets");
            foreach (var columnName in allColumnNames)
                if (!table.Columns.Contains(columnName))
                    table.Columns.Add(columnName, typeof(string));

            foreach (var asset in assets)
            {
                var row = table.NewRow();
                foreach (var key in asset.Metadata.Keys)
                    row[key] = asset.Metadata[key];

                table.Rows.Add(row);
            }
            return table;
        }
    }
}


//const string filename = "assets.xlsx";
//if (File.Exists(Path.Combine(configs.ZippedFilePath, filename)))
//    return AppendToExistingExcelDoc(assets, Path.Combine(configs.ZippedFilePath, filename));


//return CreateNewExcelDoc(assets, filename);

//private List<ITransformedMetadata> AppendToExistingExcelDoc(List<IAsset> assets, string fullPathToExcelDoc)
//{
//    var workBook = new XLWorkbook(fullPathToExcelDoc);
//    var workSheet = workBook.Worksheet(sheetName);
//    var lastUsedRow = workSheet.LastRowUsed().RowNumber();
//    workSheet.Cell(lastUsedRow + 1, 1).InsertTable(GetDataTable(assets));
//    return new List<ITransformedMetadata>();
//}

//private List<ITransformedMetadata> CreateNewExcelDoc(List<IAsset> assets, string filename)
//{
//    var workbook = new XLWorkbook();
//    var sheet = workbook.Worksheets.Add(sheetName);
//    sheet.Cell(1, 1).InsertTable(GetDataTable(assets).AsEnumerable());
//    //sheet.
//    //Grabbed this approach from https://stackoverflow.com/questions/23041021/how-to-write-some-data-to-excel-file-xlsx
//    var path = Path.Combine(Path.GetTempPath(), filename);

//    //Clean up previous run. Is only necessary if something went wrong on previous run.
//    if (File.Exists(path))
//    {
//        File.Delete(path);
//    }

//    sheet.Columns().AdjustToContents();

//    workbook.SaveAs(path);

//    return new List<ITransformedMetadata>
//    {
//        new TransformedMetadata(new FileInfo(path), filename)
//    };
//}