﻿using System;
using System.Threading.Tasks;
using ExporterWebApi.Digizuite.core;
using ExporterWebApi.Exporter.Core.Model.Mediation;
using ExporterWebApi.Interfaces;
using log4net;
using Microsoft.AspNetCore.Mvc;

namespace ExporterWebApi.Exporter.Core
{
    /// <summary>
    /// The class responsible for exporting assets
    /// </summary>
    public class Exporter : ControllerBase, IExport 
    {
        private readonly ILog _logger;
        private readonly ISearchForAssetsAndMetadata _searcher;
        private readonly IDeleteAssets _deleter;
        private readonly IDeliverAssets _assetDeliverer;
        private readonly ITransformMetadata _metadataTransformer;
        private readonly IDeliverMetadata _metadataDeliverer;

        /// <summary>
        /// Export configuration. Determines whether assets/meta data are exported, how things are exported and where they are exported to.
        /// </summary>
        public IProvideConfiguration Configs { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configs">Options for the various plugins as well as required info for export</param>
        /// <param name="searcher">Gets the information used to export like meta data, URLs, etc</param>
        /// <param name="deleter">If configured, this will delete assets after they've been exported</param>
        /// <param name="assetDeliverer">Delivers assets</param>
        /// <param name="metadataTransformer">Transforms the meta data search result</param>
        /// <param name="metadataDeliverer">Delivers the transformed meta data.</param>
        /// <param name="logger"></param>
        internal Exporter(IProvideConfiguration configs, ISearchForAssetsAndMetadata searcher, IDeleteAssets deleter, IDeliverAssets assetDeliverer, ITransformMetadata metadataTransformer, IDeliverMetadata metadataDeliverer, ILog logger)
        {
            Configs = configs;

            _logger = logger;
            _searcher = searcher;
            _deleter = deleter;
            _assetDeliverer = assetDeliverer;
            _metadataTransformer = metadataTransformer;
            _metadataDeliverer = metadataDeliverer;
        }

        /// <summary>
        /// Exports assets.
        /// </summary>
        /// <returns>A BaseResponse containing any errors, that may have occurred</returns>
        public async Task<CreateExportResponse> Export()
        {
            var total = default(int);
            var page = 0;
            SearchResult searchResult;
            try
            {
                do
                {
                    page++;
                    searchResult = await _searcher.DoSearch(page);
             
                    var assets = searchResult.Result;

                    if (total == default(int))
                    {
                        total = searchResult.Total;
                        _logger.Info($"Working on page {page}");
                    }

                    var finishedExportingToLocal = searchResult.HasError || page * Configs.AssetsPerPage > total;

                    if (assets.Count == 0 && page == 1 && !searchResult.HasError)
                    {
                        _logger.Fatal("The search didn't return any assets! So there's nothing to export. Exiting.");
                        return new CreateExportResponse()
                        {
                            ErrorCode = ErrorCode.NotFound,
                            ErrorMessage = "The search didn't return any assets! So there's nothing to export."
                        };
                    }
                    _logger.Info($"The search returned {assets.Count} assets.");

                    _logger.Debug($"Export assets: {Configs.ExportAssets}");
                    if (Configs.ExportAssets)
                    {
                        _logger.Info("Beginning export of assets.");
                        _assetDeliverer.DeliverAssets(assets, Configs, finishedExportingToLocal, searchResult.ErrorMessage);
                        _logger.Info("Done exporting assets.");
                    }

                    _logger.Info($"Export meta data: {Configs.ExportMetadata}");
                    if (Configs.ExportMetadata)
                    {
                        _logger.Info("About to export meta data");
                        var transformedMetadata = _metadataTransformer.TransformMetadata(assets, Configs);
                        _metadataDeliverer.DeliverMetadata(transformedMetadata, Configs, finishedExportingToLocal, searchResult.ErrorMessage);
                        _logger.Info("Done exporting meta data");
                    }

                    if (Configs.DeleteExportedAssets)
                    {
                        _logger.Info("About to delete exported assets");
                        _deleter.DeleteAssets(assets);
                        _logger.Info("Done deleting assets.");
                    }
                } while (page * Configs.AssetsPerPage < total && !searchResult.HasError);
            }
            catch (Exception e)
            {
                _logger.Fatal(e);
                return new CreateExportResponse {ErrorCode = ErrorCode.Unknown, ErrorMessage = e.Message};
            }

            

            _logger.Info("Done with everything!");
            return new CreateExportResponse
            {
                ErrorCode = searchResult.HasError ? ErrorCode.SearchError : ErrorCode.NoError,
                ErrorMessage = searchResult.HasError ? searchResult.ErrorMessage : null,
                DownloadUrl = Configs.DownloadExportUrl,
                FolderPath = Configs.FolderPath
            };
        }
    }
}
