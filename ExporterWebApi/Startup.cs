﻿using System;
using System.IO;
using Alexinea.Autofac.Extensions.DependencyInjection;
using Autofac;
using ExporterWebApi.Digizuite.core;
using ExporterWebApi.Exporter.Core;
using ExporterWebApi.Exporter.Core.Plugins;
using ExporterWebApi.Interfaces;
using log4net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NServiceBus;
using Swashbuckle.AspNetCore.Swagger;
using ConfigurationProvider = ExporterWebApi.Exporter.Core.Model.Configuration.ConfigurationProvider;

namespace ExporterWebApi
{
    /// <summary>
    /// The start up class. Everything is wired up here.
    /// </summary>
    public class Startup
    {
        //public Startup(IConfiguration configuration)
        //{
        //    Configuration = configuration;
        //}

        /// <summary>
        /// The constructor. 
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();


        }

        /// <summary>
        /// The property responsible for getting the configuration from apsettings.json
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSingleton(Configuration);

            services.AddScoped<IProvideConfiguration, ConfigurationProvider>();
            services.AddScoped<ISearchForAssetsAndMetadata, SearcherAndDeleter>();
            services.AddScoped<IDeleteAssets, SearcherAndDeleter>();
            services.AddScoped<ILoadPlugins, PluginLoader>();
            services.AddScoped<ISendMailAndZipFolder, MailAndZipFolderSender>();

            services.AddTransient(logger =>
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType));

            // Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Digizuite Exports API", Version = "v1" });
                c.DescribeAllEnumsAsStrings();

                var filePath = Path.Combine(AppContext.BaseDirectory, "ExporterWebApi.xml");
                c.IncludeXmlComments(filePath);
            });

            InitiateEndpoint(services);
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "api/docs/{documentName}/swagger.json";
            });
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "api/docs";
                c.SwaggerEndpoint("/api/docs/v1/swagger.json", "Digizuite Exports API - V1");
            });
        }

        /// <summary>
        /// Sets up NServicebus and its requirements
        /// </summary>
        /// <param name="services"></param>
        private static void InitiateEndpoint(IServiceCollection services)
        {
            Console.Title = "Samples.ASPNETCore.Endpoint";
            var endpointConfiguration = new EndpointConfiguration("Samples.ASPNETCore.Endpoint");

            #region Autofac and container configuratin

            var builder = new ContainerBuilder();
            builder.Populate(services);

            builder.Register(x => (IEndpointInstance) null)
                .As<IEndpointInstance>()
                .SingleInstance();

            var container = builder.Build();

            endpointConfiguration.UseContainer<AutofacBuilder>(
                customizations: customizations =>
                {
                    customizations.ExistingLifetimeScope(container);
                });

            #endregion

            #region Start Endpoint

            endpointConfiguration.UsePersistence<LearningPersistence>();
            endpointConfiguration.UseTransport<LearningTransport>().StorageDirectory("StorageDir");
            var startedEndpoint = Endpoint.Start(endpointConfiguration).GetAwaiter().GetResult();

            services.AddSingleton<IMessageSession>(startedEndpoint);

            #endregion
        }
    }
}
