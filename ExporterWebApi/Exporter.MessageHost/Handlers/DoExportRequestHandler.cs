﻿using System.Threading.Tasks;
using ExporterWebApi.Interfaces;
using ExporterWebApi.MessageHost.Messages.ExportRequest;
using log4net;
using NServiceBus;

namespace ExporterWebApi.Exporter.MessageHost.Handlers
{
    /// <summary>
    /// NServicebus ExportRequest handler
    /// </summary>
    public class DoExportRequestHandler : IHandleMessages<ExportRequest>
    {
        private readonly ILoadPlugins _pluginLoader;
        private readonly ILog _logger;// = LogManager.GetLogger(typeof(this));

        /// <summary>
        /// Sets the pluginLoader and logger
        /// </summary>
        /// <param name="pluginLoader"></param>
        /// <param name="logger"></param>
        public DoExportRequestHandler(ILoadPlugins pluginLoader, ILog logger)
        {
            _pluginLoader = pluginLoader;
            _logger = logger;
        }

        /// <summary>
        /// Handles messages to request an export
        /// </summary>
        /// <param name="message"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Handle(ExportRequest message, IMessageHandlerContext context)
        {
            var logStatement =
                $"Received a mesage to create an export. The asset Ids (if any): {(message.AssetIds != null ? string.Join(',', message.AssetIds) : "")}. The channel ID (if any): {message.ChannelId}. The catalog folder ID (if any): {message.CatalogFolderId}.";
            _logger.Debug(logStatement);

            await _pluginLoader.GetExporter(message).Export();
        }
    }
}
