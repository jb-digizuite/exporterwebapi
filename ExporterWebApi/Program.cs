﻿using System.IO;
using System.Reflection;
using log4net;
using log4net.Config;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace ExporterWebApi
{
    /// <summary>
    /// The program. Retarded comment.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The Main. Equally retarded comment
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.ConfigureAndWatch(logRepository, new FileInfo("log4net.config"));

            CreateWebHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Another retarded comment?
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
