﻿using System.Collections.Generic;
using ExporterWebApi.Enums;
using ExporterWebApi.Interfaces;

namespace ExporterWebApi.Digizuite.core
{
    /// <summary>
    /// 
    /// </summary>
    public class SearchResult
    {
        /// <summary>
        /// The total number of assets, which the search can return. Ignores paging information
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// The result of the search. Contains a list of parsed assets
        /// </summary>
        public List<IAsset> Result { get; set; }

        /// <summary>
        /// If any error occures during the search, it will be written to this property
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Defines the type of error, which occurred
        /// </summary>
        public SearchResultErrors SearchResultError { get; set; }

        /// <summary>
        /// A bool, which is true if an error occurred. False if everything's OK
        /// </summary>
        public bool HasError { get; set; }

        /// <summary>
        /// Does nothing except inialize the Result
        /// </summary>
        public SearchResult()
        {
            Result = new List<IAsset>();
        }
    }
}   
