﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ExporterWebApi.Enums;
using ExporterWebApi.Exporter.Core.Model;
using ExporterWebApi.Interfaces;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Services.Core.Digizuite.Core.Auth;

namespace ExporterWebApi.Digizuite.core
{
    internal class SearcherAndDeleter : ISearchForAssetsAndMetadata, IDeleteAssets
    {
        private readonly ILog _logger;
        private readonly IAuthService _authService;

        public IProvideConfiguration Configs { get; set; }

        /// <summary>
        /// SearcherAndDeleter constructor. Sets the configs and AuthService
        /// </summary>
        /// <param name="configs">Provides configuration like the API URL, search parameters, and more.</param>
        /// <param name="logger"></param>
        public SearcherAndDeleter(IProvideConfiguration configs, ILog logger)
        {
            Configs = configs;
            _logger = logger;
            _authService = new AuthService(configs.DamApiUrl.AbsoluteUri, "", "");
        }

        /// <summary>
        /// Get a page of search results.
        /// </summary>
        /// <param name="page">The page number to get</param>
        /// <returns></returns>
        public async Task<SearchResult> DoSearch(int page)
        {
            _logger.Debug("Invoking the search...");

            var accessKey = await GetVerifiedAccessKey();
            if (string.IsNullOrWhiteSpace(accessKey))
            {
                _logger.Fatal($"The accessKey {Configs.AccessKey} is not valid. It probably expired");
                return new SearchResult
                {
                    ErrorMessage = "The accessKey is invalid. It probably expired. Cannot execute the search.",
                    HasError = true,
                    SearchResultError = SearchResultErrors.AccessDenied,
                    Total = 0
                };
            }

            var parameters = new Dictionary<string, object>
            {
                {"searchName", Configs.SearchName},
                {"accessKey", accessKey},
                {"page", page},
                {"limit", Configs.AssetsPerPage},
                {"start", (page - 1) * Configs.AssetsPerPage}
            };

            if (Configs.ChannelId > 0)
            {
                _logger.Debug($"Searching via channelId: {Configs.ChannelId}");
                parameters.Add("channelId", Configs.ChannelId);
            }

            if (Configs.CatalogFolderId > 0)
            {
                _logger.Debug($"Searching via catalogFolderId: {Configs.CatalogFolderId}");
                parameters.Add("catalogFolderId", Configs.CatalogFolderId);
            }

            if (Configs.AssetIds != null && Configs.AssetIds.Count > 0)
            {
                _logger.Debug($"Searching via assetIds: {string.Join(',', Configs.AssetIds)}");

                foreach (int assetId in Configs.AssetIds)
                    parameters.Add("assetIds", assetId);

                if (Configs.AssetIds.Count > 1)
                    parameters.Add("assetIds_type_multiids", 1);
            }

            var response = DoRequest("searchservice.js", Method.GET, parameters);

            try
            {

                var jsonObj = JObject.Parse(response.Content);
                var success = Convert.ToBoolean(jsonObj.SelectTokens("$.success").FirstOrDefault());
                if (!success)
                {
                    _logger.Fatal($"The search returned success = false. The response.Content: {response.Content}");
                    var warnings = jsonObj.SelectToken("warnings", true).Children().ToList();
                    var description = warnings[0].SelectToken("Description", true).ToString();
                    return new SearchResult
                    {
                        ErrorMessage = description,
                        HasError = true,
                        SearchResultError = SearchResultErrors.ApiError
                    };
                }

                _logger.Info("Successfully got the search result.");
                _logger.Debug($"The search result: {response.Content}");

                return new SearchResult
                {
                    HasError = false,
                    Result = ParseAssets(jsonObj.SelectToken("items", true).Children().ToList()),
                    Total = Convert.ToInt32(jsonObj.SelectTokens($"$.{Configs.TotalFieldId}").FirstOrDefault())
                };
            }
            catch (JsonReaderException)
            {
                _logger.Fatal($"Could not parse the response from the search request.\nThe HTTP status code: {response.StatusCode}. Status code description: {response.StatusDescription}\nResponse.Content: {response.Content}");
                return new SearchResult
                {
                    HasError = false,
                    Total = 0,
                    ErrorMessage = "Could not parse search result.",
                    SearchResultError = SearchResultErrors.ResultParseError
                };
            }
        }

        /// <summary>
        /// Makes a soft deletion of the suplied assets.
        /// </summary>
        /// <param name="assets">The assets to delete</param>
        public void DeleteAssets(List<IAsset> assets)
        {
            var itemIds = new List<int>();
            foreach (IAsset asset in assets)
                itemIds.Add(asset.ItemId);

            _logger.Debug($"About to soft delete exported assets. The item IDs: {string.Join(',', itemIds)}");

            var accessKey = GetVerifiedAccessKey().Result;
            if (string.IsNullOrWhiteSpace(accessKey))
            {
                _logger.Fatal($"The accessKey {Configs.AccessKey} is not valid. It probably expired. Cannot delete assets");
                return;
            }

            var parameters = new Dictionary<string, object>
            {
                {"updateXML", "<r><asset fieldId=\"BatchUpdateDelete\"><empty fieldId=\"empty\"/></asset></r>"},
                {"values", "[{\"Id\":\"BatchUpdateDelete\",\"FieldId\":\"BatchUpdateDelete\",\"ContainerType\":6,\"RowId\":1,\"Values\":[{\"FieldId\":\"empty\",\"Type\":3,\"Values\":[0]}],\"ItemIds\":[" + string.Join(",", itemIds) + "]}]"},
                {"useVersionedMetadata", "false"},
                {"accessKey", accessKey}
            };

            //var parameters = new List<KeyValuePair<string, object>>
            //{
            //    new KeyValuePair<string, object>("updateXML",
            //        "<r><asset fieldId=\"BatchUpdateDelete\"><empty fieldId=\"empty\"/></asset></r>"),
            //    new KeyValuePair<string, object>("values",
            //        "[{\"Id\":\"BatchUpdateDelete\",\"FieldId\":\"BatchUpdateDelete\",\"ContainerType\":6,\"RowId\":1,\"Values\":[{\"FieldId\":\"empty\",\"Type\":3,\"Values\":[0]}],\"ItemIds\":[" +
            //        string.Join(",", itemIds) + "]}]"),
            //    new KeyValuePair<string, object>("useVersionedMetadata", "false"),
            //    new KeyValuePair<string, object>("accessKey", accessKey)
            //};

            var response = DoRequest("BatchUpdateService.js", Method.POST, parameters);

            try
            {

                var jsonObj = JObject.Parse(response.Content);
                var success = Convert.ToBoolean(jsonObj.SelectTokens("$.success").FirstOrDefault());
                if (!success)
                    _logger.Fatal($"The request to delte assets returned sucess = false. The response.Content: {response.Content}");

                _logger.Debug($"Result from delete-assets request: {response.Content}");
            }
            catch (JsonReaderException)
            {
                _logger.Fatal($"Could not parse the response from the search request.\nThe HTTP status code: {response.StatusCode}. Status code description: {response.StatusDescription}\nResponse.Content: {response.Content}");
            }
        }

        private async Task<string> GetVerifiedAccessKey()
        {
            var authHeader = new AuthenticationHeaderValue("AccessKey", Configs.AccessKey);

            try
            {
                return await _authService.IsLoggedIn(authHeader) ? Configs.AccessKey : null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private IRestResponse DoRequest(string service, Method verb, Dictionary<string, object> parameters)
        {
            _logger.Debug($"Executing HTTP resquest. service: {service}, verb: {verb}, The parameters:\n{string.Join(",\n", parameters.Select(kv => kv.Key + "=" + kv.Value))}");

            var uri = Configs.DamApiUrl;
            var baseUrl = $"{uri.Scheme}://{uri.DnsSafeHost}";
            var path = uri.LocalPath;

            var client = new RestClient(baseUrl);

            var request = new RestRequest($"{path}/{service}", verb) { Timeout = 20000 };

            foreach (KeyValuePair<string, object> parameter in parameters)
                request.AddParameter(parameter.Key, parameter.Value);

            return client.Execute(request);
        }

        private List<IAsset> ParseAssets(List<JToken> items)
        {
            var assets = new List<IAsset>();

            foreach (var item in items)
                assets.Add(new Asset((JObject)item, Configs));

            return assets;
        }
    }
}
