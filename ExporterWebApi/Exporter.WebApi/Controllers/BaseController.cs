﻿using System;
using ExporterWebApi.Exporter.Core.Model.Mediation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ExporterWebApi.Exporter.WebApi.Controllers
{
    /// <summary>
    /// Helper class. Helps in sending responses back to the client
    /// </summary>
    public class BaseController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="response"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        protected IActionResult MapToResult<TResponse>(TResponse response, Func<TResponse, IActionResult> result)
            where TResponse : BaseResponse
        {
            return !response.HasError ? result(response)
                : Error(response);
        }

        private IActionResult Error(BaseResponse response)
        {
            int statusCode;
            switch (response.ErrorCode)
            {
                case ErrorCode.NotValid:
                case ErrorCode.SearchError:
                    statusCode = StatusCodes.Status400BadRequest;
                    break;
                case ErrorCode.NotFound:
                    statusCode = StatusCodes.Status404NotFound;
                    break;
                case ErrorCode.Unknown:
                    statusCode = StatusCodes.Status500InternalServerError;
                    break;
                default:
                    throw new Exception(string.Format("Invalid ErrorCode '{0}', cannot return error", response.ErrorCode));
            }

            return StatusCode(statusCode, response);
        }
    }
}
