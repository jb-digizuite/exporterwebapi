﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ExporterWebApi.Enums;

namespace ExporterWebApi.Exporter.WebApi.Controllers.RequestModel
{
    /// <summary>
    /// The EXporter API base request model. Every other request inherites from this
    /// </summary>
    public class BaseRequest
    {
        /// <summary>
        /// The different possibilities when transforming meta data. Defaults to "xml"
        /// </summary>
        [Required]
        [EnumValidator(typeof(MetadataTransformType))]
        [Display(Name = "metadataTransformType")]
        public MetadataTransformType? MetadataTransformType { get; set; }

        /// <summary>
        /// Determines whether assets are exported. Defaults to true
        /// </summary>
        [Required]
        [Display(Name = "exportAssets")]
        public bool? ExportAssets { get; set; }

        /// <summary>
        /// Determines whether meta data is exported. Defaults to true
        /// </summary>
        [Required]
        [Display(Name = "exportMetadata")]
        public bool? ExportMetadata { get; set; }

        /// <summary>
        /// Determines whether the exported assets should be "soft deleted". Defaults to false
        /// </summary>
        [Required]
        [Display(Name = "deleteExportedAssets")]
        public bool? DeleteExportedAssets { get; set; }

        /// <summary>
        /// Search2 search XML valueField IDs to exclude from the extracted meta data
        /// </summary>
        [Required]
        [Display(Name = "metadataExludeFieldIds")]
        public List<string> MetadataExludeFieldIds { get; set; }

        /// <summary>
        /// Authenticates a user when the API retrieves a request. Also used to authorize the read access to the assets and meta data when executing the search
        /// </summary>
        [Required]
        [Display(Name = "accessKey")]
        public string AccessKey { get; set; }
    }
}
