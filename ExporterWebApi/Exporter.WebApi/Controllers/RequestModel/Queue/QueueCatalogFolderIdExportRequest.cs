﻿using System.ComponentModel.DataAnnotations;

namespace ExporterWebApi.Exporter.WebApi.Controllers.RequestModel.Queue
{
    /// <summary>
    /// Used when requesting a queued export of a catalog folder
    /// </summary>
    public class QueueCatalogFolderIdExportRequest : BaseQueueRequest
    {
        /// <summary>
        /// Assets within the specified catalog folder will be exported
        /// </summary>
        [Required]
        [IntSizeValidation(1)]
        [Display(Name = "catalogFolderId")]
        public int CatalogFolderId { get; set; }
    }
}
