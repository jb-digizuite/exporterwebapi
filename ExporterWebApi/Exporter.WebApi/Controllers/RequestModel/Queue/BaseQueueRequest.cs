﻿using System.ComponentModel.DataAnnotations;

namespace ExporterWebApi.Exporter.WebApi.Controllers.RequestModel.Queue
{
    /// <summary>
    /// The base of the requests used when an export should be queued
    /// </summary>
    public class BaseQueueRequest : BaseRequest
    {
        /// <summary>
        /// The mail address used to tell the client, that the export has been created and is ready for download
        /// </summary>
        [Required]
        [EmailAddress]
        [Display(Name = "mailAddress")]
        public string MailAddress { get; set; }
    }
}
