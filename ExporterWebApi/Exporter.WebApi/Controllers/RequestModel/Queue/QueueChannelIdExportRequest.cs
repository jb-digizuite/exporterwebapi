﻿using System.ComponentModel.DataAnnotations;

namespace ExporterWebApi.Exporter.WebApi.Controllers.RequestModel.Queue
{
    /// <summary>
    /// Used when requesting a queued export of a channel
    /// </summary>
    public class QueueChannelIdExportRequest : BaseQueueRequest
    {
        /// <summary>
        /// Assets within the specified channel will be exported
        /// </summary>
        [Required]
        [IntSizeValidation(1)]
        [Display(Name = "channelId")]
        public int ChannelId { get; set; }
    }
}
