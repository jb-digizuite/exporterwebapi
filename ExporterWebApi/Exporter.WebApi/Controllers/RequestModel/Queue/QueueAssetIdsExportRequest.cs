﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ExporterWebApi.Exporter.WebApi.Controllers.RequestModel.Queue
{
    /// <summary>
    /// Used when requesting a queued export of specified asset Ids
    /// </summary>
    public class QueueAssetIdsExportRequest : BaseQueueRequest
    {
        /// <summary>
        /// Assets with the specified asset IDs will be exported
        /// </summary>
        [Required, MinLength(1, ErrorMessage = "\"assetIds\" must contain at least 1 asset ID")]
        [Display(Name = "assetIds")]
        public List<int> AssetIds { get; set; }
    }
}
