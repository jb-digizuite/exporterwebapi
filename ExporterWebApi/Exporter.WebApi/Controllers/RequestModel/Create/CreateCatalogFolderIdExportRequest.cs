﻿using System.ComponentModel.DataAnnotations;

namespace ExporterWebApi.Exporter.WebApi.Controllers.RequestModel.Create
{
    /// <summary>
    /// Used when an export of a specific catalog folder is requested
    /// </summary>
    public class CreateCatalogFolderIdExportRequest : BaseRequest
    {
        /// <summary>
        /// The catalog folder ID of the catalog folder, whose assets should be exported
        /// </summary>
        [Required]
        [IntSizeValidation(1)]
        [Display(Name = "catalogFolderId")]
        public int CatalogFolderId { get; set; }
    }
}
