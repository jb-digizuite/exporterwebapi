﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ExporterWebApi.Exporter.WebApi.Controllers.RequestModel.Create
{
    /// <summary>
    /// Used when an export of specific asset IDs is requested
    /// </summary>
    public class CreateAssetIdsExportRequest : BaseRequest
    {
        /// <summary>
        /// The asset IDs belonging to the assets, which should be exported
        /// </summary>
        [Required, MinLength(1, ErrorMessage = "\"assetIds\" must contain at least 1 asset ID")]
        [Display(Name = "assetIds")]
        public List<int> AssetIds { get; set; }
    }
}
