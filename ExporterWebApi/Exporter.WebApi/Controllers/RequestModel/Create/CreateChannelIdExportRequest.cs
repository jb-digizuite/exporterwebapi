﻿using System.ComponentModel.DataAnnotations;

namespace ExporterWebApi.Exporter.WebApi.Controllers.RequestModel.Create
{
    /// <summary>
    /// Used when an export of a specific channel is requested
    /// </summary>
    public class CreateChannelIdExportRequest : BaseRequest
    {
        /// <summary>
        /// The channel ID of the channel, whose assets should be exported
        /// </summary>
        [Required]
        [IntSizeValidation(1)]
        [Display(Name = "channelId")]
        public int ChannelId { get; set; }
    }
}




