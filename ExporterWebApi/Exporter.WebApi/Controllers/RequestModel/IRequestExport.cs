﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExporterWebApi.Enums;

namespace ExporterWebApi.Exporter.WebApi.Controllers.RequestModel
{
    public interface IRequestExport
    {
        MetadataTransformType MetadataTransformType { get; set; }
        bool ExportAssets { get; set; }
        bool ExportMetadata { get; set; }
        bool DeleteExportedAssets { get; set; }
        string MetadataExludeFieldIds { get; set; }
        User User { get; set; }
    }
}
