﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using ExporterWebApi.Exporter.Core.Model.Mediation;
using ExporterWebApi.Exporter.WebApi.Controllers.RequestModel;
using ExporterWebApi.Exporter.WebApi.Controllers.RequestModel.Create;
using ExporterWebApi.Exporter.WebApi.Controllers.RequestModel.Queue;
using ExporterWebApi.Interfaces;
using ExporterWebApi.MessageHost.Messages.ExportRequest;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NServiceBus;

namespace ExporterWebApi.Exporter.WebApi.Controllers.v1
{
    /// <summary>
    /// The class responsible for handling incoming export and download requests 
    /// </summary>
    [TypeFilter(typeof(AccessKeyAuthenticator))]
    [Route("api/exports")]
    [ApiController]
    public class ExportsController : BaseController 
    {
        private readonly ILoadPlugins _pluginLoader;
        private readonly IMessageSession _messageSession;
        private readonly ILog _logger;
        private readonly ISendMailAndZipFolder _folderZipper;

        /// <summary>
        /// Sets up all the dependencies
        /// </summary>
        /// <param name="pluginLoader">Gets the Exporter</param>
        /// <param name="messageSession">The NServicebus endpoint</param>
        /// <param name="logger"></param>
        /// <param name="folderZipper"></param>
        public ExportsController(ILoadPlugins pluginLoader, IMessageSession messageSession, ILog logger, ISendMailAndZipFolder folderZipper)
        {
            _pluginLoader = pluginLoader;
            _messageSession = messageSession;
            _logger = logger;
            _folderZipper = folderZipper;
        }

        /// <summary>
        /// Queues an export of specific asset IDs
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Export has been queued</response>
        /// <response code="400">One or more required parameters are missing</response>
        /// <response code="403">The accessKey parameter is invalid</response>
        /// <response code="404">The search for assets to export returned nothing</response>
        /// <response code="500">An unexpected error occurred</response>
        [HttpPost("assetIds")]
        public IActionResult QueueAssetIdsExport([FromBody] QueueAssetIdsExportRequest request)
        {
            SetLog4netGuid();
            _logger.Debug($"Received a request to queue an export of asset IDs. The asset IDs: {string.Join(',', request.AssetIds)}. AccessKey used for requesting an export: {request.AccessKey}");

            _messageSession.SendLocal(GetQueueMessageFromRequest(request));

            return MapToResult(new QueuedResponse(), Ok);
        }

        /// <summary>
        /// Queues an export of a catalog folder's asset
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Export has been queued</response>
        /// <response code="400">One or more required parameters are missing</response>
        /// <response code="403">The accessKey parameter is invalid</response>
        /// <response code="404">The search for assets to export returned nothing</response>
        /// <response code="500">An unexpected error occurred</response>
        [HttpPost("catalogFolderId")]
        public IActionResult QueueCatalogFolderExport([FromBody] QueueCatalogFolderIdExportRequest request)
        {
            SetLog4netGuid();
            _logger.Debug($"Received a request to queue an export of a catalog folder. The catalog folder ID: {request.CatalogFolderId}. AccessKey used for requesting an export: {request.AccessKey}");

            _messageSession.SendLocal(GetQueueMessageFromRequest(request));

            return MapToResult(new QueuedResponse(), Ok);
        }

        /// <summary>
        /// Queues an export of a channel's assets
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Export has been queued</response>
        /// <response code="400">One or more required parameters are missing</response>
        /// <response code="403">The accessKey parameter is invalid</response>
        /// <response code="404">The search for assets to export returned nothing</response>
        /// <response code="500">An unexpected error occurred</response>
        [HttpPost("channelId")]
        public IActionResult QueueChannelIdExport([FromBody] QueueChannelIdExportRequest request)
        {
            SetLog4netGuid();
            _logger.Debug($"Received a request to queue an export of a channel. The channel ID: {request.ChannelId}. AccessKey used for requesting an export: {request.AccessKey}");

            _messageSession.SendLocal(GetQueueMessageFromRequest(request));

            return MapToResult(new QueuedResponse(), Ok);
        }

        /// <summary>
        /// Creates an export of spicific asset IDs
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Export has been created and is ready for download</response>
        /// <response code="400">One or more required parameters are missing</response>
        /// <response code="403">The accessKey parameter is invalid</response>
        /// <response code="404">The search for assets to export returned nothing</response>
        /// <response code="500">An unexpected error occurred</response>
        [HttpPost("downloads/assetIds")]
        public async Task<IActionResult> CreateAssetIdsExport([FromBody] CreateAssetIdsExportRequest request)
        {
            SetLog4netGuid();
            _logger.Debug($"Received a request to create an export of asset IDs. The asset IDs: {string.Join(',', request.AssetIds)}. AccessKey used for requesting an export: {request.AccessKey}");

            return MapToResult(await _pluginLoader.GetExporter(request).Export(),
                ZipFileCopyToStorageAndReturnOk);
        }

        /// <summary>
        /// Create an export of a catalog folder's assets
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Export has been created and is ready for download</response>
        /// <response code="400">One or more required parameters are missing</response>
        /// <response code="403">The accessKey parameter is invalid</response>
        /// <response code="404">The search for assets to export returned nothing</response>
        /// <response code="500">An unexpected error occurred</response>
        [HttpPost("downloads/catalogFolderId")]
        public async Task<IActionResult> CreateCatalogFolderExport([FromBody] CreateCatalogFolderIdExportRequest request)
        {
            SetLog4netGuid();
            _logger.Debug($"Received a request to create an export of a catalog folder. The catalog folder ID: {request.CatalogFolderId}. AccessKey used for requesting an export: {request.AccessKey}");

            return MapToResult(await _pluginLoader.GetExporter(request).Export(),
                ZipFileCopyToStorageAndReturnOk);
        }

        /// <summary>
        /// Creates an export of a channel's assets
        /// </summary>
        /// <param name="request"></param>
        /// <response code="200">Export has been created and is ready for download</response>
        /// <response code="400">One or more required parameters are missing</response>
        /// <response code="403">The accessKey parameter is invalid</response>
        /// <response code="404">The search for assets to export returned nothing</response>
        /// <response code="500">An unexpected error occurred</response>
        [HttpPost("downloads/channelId")]
        public async Task<IActionResult> CreateChannelExport([FromBody] CreateChannelIdExportRequest request)
        {
            SetLog4netGuid();
            _logger.Debug($"Received a request to create an export of a channel. The catalog folder ID: {request.ChannelId}. AccessKey used for requesting an export: {request.AccessKey}");

            return MapToResult(await _pluginLoader.GetExporter(request).Export(),
                ZipFileCopyToStorageAndReturnOk);
        }

        /// <summary>
        /// Downloads a specific export
        /// </summary>
        /// <param name="guid">Identifies the export</param>
        /// <param name="accessKey">Authenticates a user</param>
        /// <param name="deleteExport">Defaults to true. Determines whether the export should be deleted after download</param>
        /// <response code="200">Export is downloading</response>
        /// <response code="400">One or more required parameters are missing</response>
        /// <response code="403">The accessKey parameter is invalid</response>
        /// <response code="404">The search for assets to export returned nothing</response>
        /// <response code="500">An unexpected error occurred</response>
        [HttpGet("{guid}")]
        public IActionResult GetExport(Guid guid, [Required][FromQuery] string accessKey, [FromQuery] bool deleteExport = true)
        {
            SetLog4netGuid();
            _logger.Debug($"Received request to download an export. Guid {guid}, acccessKey {accessKey}");

            return SetupDownloadOfZipFile(guid.ToString(), deleteExport);
        }

        #region Helper methods

        private IExportRequest GetQueueMessageFromRequest(BaseRequest request)
        {
            string mailAdress = ((BaseQueueRequest) request).MailAddress;
            int channelId = 0, catalogFolderId = 0;
            List<int> assetIds = null;

            switch (request)
            {
                case QueueCatalogFolderIdExportRequest queueCatalogFolderIdExportRequest:
                    catalogFolderId = queueCatalogFolderIdExportRequest.CatalogFolderId;
                    break;
                case QueueChannelIdExportRequest queueChannelIdExportRequest:
                    channelId = queueChannelIdExportRequest.ChannelId;
                    break;
                case QueueAssetIdsExportRequest queueAssetIdsExportRequest:
                    assetIds = queueAssetIdsExportRequest.AssetIds;
                    break;
            }

            return new ExportRequest
            {
                AccessKey = request.AccessKey,
                MetadataExludeFieldIds = request.MetadataExludeFieldIds,
                MetadataTransformType = request.MetadataTransformType.ToString(),
                MailAddress = mailAdress,

                ChannelId = channelId,
                CatalogFolderId = catalogFolderId,
                AssetIds = assetIds,
                ExportMetadata = request.ExportMetadata ?? true,
                ExportAssets = request.ExportAssets ?? true,
                DeleteExportedAssets = request.DeleteExportedAssets ?? false
            };
        }

        private void SetLog4netGuid()
        {
            LogicalThreadContext.Properties["guid"] = Guid.NewGuid();
        }

        private IActionResult SetupDownloadOfZipFile(string uniqueFolderName, bool deleteExport)
        {
            try
            {
                var bytes = _folderZipper.GetZippedExportFromStorage(uniqueFolderName, deleteExport);

                return File(bytes, "application/zip", "Exports.zip");
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new BaseResponse
                {
                    ErrorCode = ErrorCode.Unknown,
                    ErrorMessage = $"An unexpected error occurred: {e.Message}"
                });
            }
        }

        private IActionResult ZipFileCopyToStorageAndReturnOk(CreateExportResponse result)
        {

            try
            {
                _folderZipper.ZipAndMoveExportToStorage(result.FolderPath);
            }
            catch (Exception e)
            {
                _logger.Fatal(e);
                return StatusCode(StatusCodes.Status500InternalServerError, new BaseResponse
                {
                    ErrorCode = ErrorCode.Unknown,
                    ErrorMessage = $"An error occurred while zipping and moving the export to DAM storage: {e.Message}"
                });
            }

            return new OkObjectResult(result);
        }
        #endregion  

        private class QueuedResponse : BaseResponse
        {
            // ReSharper disable once UnusedMember.Local
            public string Message { get; set; } = "Your export has been queued.";
        }
    }
}
