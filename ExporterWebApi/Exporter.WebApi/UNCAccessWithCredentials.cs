using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using log4net;
using DWORD = System.UInt32;
using LPWSTR = System.String;
using NET_API_STATUS = System.UInt32;

namespace ExporterWebApi.Exporter.WebApi
{
    public class UncAccessWithCredentials : IDisposable
    {
        //[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        //internal struct USE_INFO_2
        //{
        //    internal LPWSTR ui2_local;
        //    internal LPWSTR ui2_remote;
        //    internal LPWSTR ui2_password;
        //    internal DWORD ui2_status;
        //    internal DWORD ui2_asg_type;
        //    internal DWORD ui2_refcount;
        //    internal DWORD ui2_usecount;
        //    internal LPWSTR ui2_username;
        //    internal LPWSTR ui2_domainname;
        //}

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct USE_INFO_2
        {
            public string ui2_local;
            public string ui2_remote;
            public string ui2_password;
            public UInt32 ui2_status;
            public UInt32 ui2_asg_type;
            public UInt32 ui2_refcount;
            public UInt32 ui2_usecount;
            public string ui2_username;
            public string ui2_domainname;
        }

        //[DllImport("NetApi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        //internal static extern NET_API_STATUS NetUseAdd(
        //    LPWSTR UncServerName,
        //    DWORD Level,
        //    ref USE_INFO_2 Buf,
        //    out DWORD ParmError);
        [DllImport("NetApi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern uint NetUseAdd(
            string UncServerName,
            UInt32 Level,
            ref USE_INFO_2 Buf,
            out UInt32 ParmError
        );

        [DllImport("NetApi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern NET_API_STATUS NetUseDel(
            LPWSTR UncServerName,
            LPWSTR UseName,
            DWORD ForceCond);

        private bool disposed = false;

        private string sUNCPath;
        private string sUser;
        private string sPassword;
        private string sDomain;
        private int iLastError;

        private readonly ILog _logger;

        /// <summary>
        /// A disposeable class that allows access to a UNC resource with credentials.
        /// </summary>
        public UncAccessWithCredentials(ILog logger)
        {
            if (logger == null)
                logger = LogManager.GetLogger(typeof(UncAccessWithCredentials));

            _logger = logger;
        }

        /// <summary>
        /// The last system error code returned from NetUseAdd or NetUseDel.  Success = 0
        /// </summary>
        public int LastError
        {
            get { return iLastError; }
        }

        public void Dispose()
        {
            if (!this.disposed)
            {
                NetUseDelete();
            }
            disposed = true;
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Connects to a UNC path using the credentials supplied.
        /// </summary>
        /// <param name="UNCPath">Fully qualified domain name UNC path</param>
        /// <param name="User">A user with sufficient rights to access the path.</param>
        /// <param name="Domain">Domain of User.</param>
        /// <param name="Password">Password of User</param>
        /// <returns>True if mapping succeeds.  Use LastError to get the system error code.</returns>
        public bool NetUseWithCredentials(string UNCPath, string User, string Domain, string Password)
        {
            sUNCPath = UNCPath;
            sUser = User;
            sPassword = Password;
            sDomain = Domain;
            return NetUseWithCredentials();
        }

        private bool NetUseWithCredentials()
        {
            uint returncode;
            try
            {
                //USE_INFO_2 useinfo = new USE_INFO_2();

                //useinfo.ui2_remote = sUNCPath;
                //useinfo.ui2_username = sUser;
                //useinfo.ui2_domainname = sDomain;
                //useinfo.ui2_password = sPassword;
                //useinfo.ui2_asg_type = 0;
                //useinfo.ui2_usecount = 1;
                //uint paramErrorIndex;
                //returncode = NetUseAdd(null, 2, ref useinfo, out paramErrorIndex);
                //iLastError = (int)returncode;

                USE_INFO_2 info = new USE_INFO_2();
                info.ui2_local = null;
                info.ui2_asg_type = 0xFFFFFFFF;
                info.ui2_remote = sUNCPath;
                info.ui2_username = sUser;
                info.ui2_password = sPassword;
                info.ui2_domainname = sDomain;

                uint returnCode = NetUseAdd(null, 2, ref info, out var paramErrorIndex);

                if (returnCode != 0)
                {
                    throw new Win32Exception((int)returnCode);
                }

                return true;
            }
            catch(Exception ex)
            {
                iLastError = Marshal.GetLastWin32Error();
                _logger.Fatal(ex.Message, ex);
                return false;
            }
        }

        /// <summary>
        /// Ends the connection to the remote resource 
        /// </summary>
        /// <returns>True if it succeeds.  Use LastError to get the system error code</returns>
        public bool NetUseDelete()
        {
            uint returncode;
            try
            {
                returncode = NetUseDel(null, sUNCPath, 2);
                iLastError = (int)returncode;
                return (returncode == 0);
            }
            catch
            {
                iLastError = Marshal.GetLastWin32Error();
                return false;
            }
        }

        ~UncAccessWithCredentials()
        {
            Dispose();
        }

    }
}
