﻿using System;
using System.Net.Http.Headers;
using ExporterWebApi.Exporter.Core.Model.Mediation;
using ExporterWebApi.Exporter.WebApi.Controllers.RequestModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Services.Core.Digizuite.Core.Auth;

namespace ExporterWebApi.Exporter.WebApi
{
    /// <summary>
    /// Authenticates access to the Exporter API
    /// </summary>
    public class AccessKeyAuthenticator : IActionFilter
    {
        private readonly IAuthService _authService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configs">Only used to get the "damApiUrl" from appsettings</param>
        public AccessKeyAuthenticator(IConfiguration configs)
        {
            _authService = new AuthService(configs.GetSection("AppConfiguration")["damApiUrl"], "", "");    
        }

        /// <summary>
        /// Hooks into the ASp.Net Core pipeline to intercept all requests before they hit the Exports controller.
        /// Invalidates the request and responds with 403 if the accessKey parameter is missing or invalid
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                string accessKey;
            
                if (context.ActionArguments.ContainsKey("request"))
                    accessKey = ((BaseRequest)context.ActionArguments["request"]).AccessKey;
                else if (context.ActionArguments.ContainsKey("accessKey"))
                    accessKey = context.ActionArguments["accessKey"].ToString();
                else
                {
                    SetContextAsFailed(context, "Could not find the \"accessKey\" parameter.", StatusCodes.Status400BadRequest);
                    return;
                }


                if (_authService.IsLoggedIn(new AuthenticationHeaderValue("AccessKey", accessKey)).Result)
                    return;

                SetContextAsFailed(context, $"The accessKey parameter value \"{accessKey}\" is not valid.");
            }
            catch (Exception e)
            {
                SetContextAsFailed(context, $"An unknown error occurred while attempting to authenticate the \"accessKey\" parameter: {e.Message}");
            }
        }

        /// <summary>
        /// Hooks into the ASP.Net Core pipeline to intercept all responses. 
        /// It's not used for anything. It's only here because of the interface
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuted(ActionExecutedContext context)
        {
            
        }

        private void SetContextAsFailed(ActionExecutingContext context, string errorMessage, int httpStatusCode = StatusCodes.Status403Forbidden)
        {
            context.HttpContext.Response.StatusCode = httpStatusCode;
            context.HttpContext.Response.Headers.Clear();

            context.Result = new JsonResult(new BaseResponse
            {
                ErrorCode = ErrorCode.NotValid,
                ErrorMessage = errorMessage
            });
        }
    }
}
