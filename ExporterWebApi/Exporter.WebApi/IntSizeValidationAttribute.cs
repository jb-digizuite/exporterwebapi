﻿using System.ComponentModel.DataAnnotations;

namespace ExporterWebApi.Exporter.WebApi
{
    /// <summary>
    /// Validates the size of an int
    /// </summary>
    public class IntSizeValidationAttribute : ValidationAttribute
    {
        private readonly int _minimumValue;

        /// <summary>
        /// Sets the minimum size
        /// </summary>
        /// <param name="minimumValue"></param>
        public IntSizeValidationAttribute(int minimumValue)
        {
            _minimumValue = minimumValue;
        }

        /// <summary>
        /// Validates whether an int's size is larger or equal to the minimum size
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="validationContext">Contains information about the context</param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (int.TryParse(value.ToString(), out var parsedValue) && parsedValue >= _minimumValue)
                return ValidationResult.Success;

            if (!string.IsNullOrWhiteSpace(ErrorMessage))
                return new ValidationResult(ErrorMessage);

            return new ValidationResult($"{validationContext.DisplayName} must be bigger than {_minimumValue - 1}");
        }
    }
}
