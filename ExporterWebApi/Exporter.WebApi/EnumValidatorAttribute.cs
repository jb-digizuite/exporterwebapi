﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ExporterWebApi.Exporter.WebApi
{
    /// <summary>
    /// Validates enums
    /// </summary>
    public class EnumValidatorAttribute : ValidationAttribute
    {
        private readonly Type _type;

        /// <summary>
        /// Sets the enum type
        /// </summary>
        /// <param name="type"></param>
        public EnumValidatorAttribute(Type type)
        {
            _type = type;
        }

        /// <inheritdoc />
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null && Enum.IsDefined(_type, value))
                return ValidationResult.Success;

            if (!string.IsNullOrWhiteSpace(ErrorMessage))
                return new ValidationResult(ErrorMessage);

            var validValues = Enum.GetValues(_type);

            var validEnums = "";
            var validInts = "";
            foreach (var validValue in validValues)
            {
                validEnums += $"\"{validValue}\", ";
                validInts += $"{(int)validValue}, ";
            }

            validEnums = validEnums.TrimEnd(',', ' ');
            validInts = validInts.TrimEnd(',', ' ');

            return new ValidationResult($"{validationContext.DisplayName} is invalid. The valid string values are {validEnums}. The valid int values are {validInts}");
        }
    }
}
