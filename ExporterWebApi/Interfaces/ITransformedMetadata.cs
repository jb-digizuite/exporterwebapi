﻿namespace ExporterWebApi.Interfaces
{
    /// <summary>
    /// Defines the object used to transfer the transformed meta data to its deliverer
    /// </summary>
    public interface ITransformedMetadata
    {
        /// <summary>
        /// Typically holds a single asset's meta data. Excel with Excel, where all assets' meta data is placed here
        /// </summary>
        object Metadata { get; }
        
        /// <summary>
        /// The file name of the transformed asset meta data document
        /// </summary>
        string Filename { get; }
    }
}
