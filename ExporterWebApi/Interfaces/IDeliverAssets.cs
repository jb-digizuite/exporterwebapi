﻿using System.Collections.Generic;

namespace ExporterWebApi.Interfaces
{
    internal interface IDeliverAssets
    {
        void DeliverAssets(List<IAsset> assets, IProvideConfiguration configs, bool sendExports, string errorMesage);
    }
}
