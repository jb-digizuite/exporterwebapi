﻿using System.Threading.Tasks;
using ExporterWebApi.Exporter.Core.Model.Mediation;

namespace ExporterWebApi.Interfaces
{
    /// <summary>
    /// Defines an Exporter
    /// </summary>
    public interface IExport
    {
        /// <summary>
        /// Export configuration. Determines whether assets/meta data are exported, how things are exported and where they are exported to.
        /// </summary>
        IProvideConfiguration Configs { get; set; }

        /// <summary>
        /// Exports assets.
        /// </summary>
        /// <returns>A BaseResponse containing any errors, that may have occurred</returns>
        Task<CreateExportResponse> Export();
    }
}
