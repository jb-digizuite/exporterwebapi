﻿using System.Collections.Generic;

namespace ExporterWebApi.Interfaces
{
    internal interface ITransformMetadata
    {
        /// <summary>
        /// Transforms the meta data into a document format like XML or Excel
        /// </summary>
        /// <param name="assets">The objects containing the meta data to transform</param>
        /// <param name="configs">May contain info used to transform the meta data</param>
        /// <returns></returns>
        List<ITransformedMetadata> TransformMetadata(List<IAsset> assets, IProvideConfiguration configs);
    }
}
