﻿using ExporterWebApi.Exporter.WebApi.Controllers.RequestModel;
using ExporterWebApi.MessageHost.Messages.ExportRequest;

namespace ExporterWebApi.Interfaces
{
    /// <summary>
    /// Defines a PluginLoader
    /// </summary>
    public interface ILoadPlugins
    {
        /// <summary>
        /// Determines what plugins should be used
        /// </summary>
        IProvideConfiguration Configs { get; set; }

        /// <summary>
        /// Gets an exporter instance with the plugins specified by the Configs propery.
        /// </summary>
        /// <param name="request">Request model from an HTTP request</param>
        /// <returns></returns>
        IExport GetExporter(BaseRequest request);

        /// <summary>
        /// Gets an exporter instance with the plugins specified by the Configs propery.
        /// </summary>
        /// <param name="request">Request model for an NServicebus message</param>
        /// <returns></returns>
        IExport GetExporter(ExportRequest request);
    }
}
