﻿using System.Collections.Generic;

namespace ExporterWebApi.Interfaces
{
    internal interface IDeliverMetadata
    {
        /// <summary>
        /// Delivers meta data
        /// </summary>
        /// <param name="metadata">The meta data to deliver</param>
        /// <param name="configs">Determines where/how the meta data is delivered</param>
        /// <param name="sendExports">True if the the supplied meta data is the last to be delivered</param>
        /// <param name="errorMesage">Any eventual error message. Used when delivering the export to somewhere other then the client.</param>
        void DeliverMetadata(List<ITransformedMetadata> metadata, IProvideConfiguration configs, bool sendExports, string errorMesage);   
    }
}
