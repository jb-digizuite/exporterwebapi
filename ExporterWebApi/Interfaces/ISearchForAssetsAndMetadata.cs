﻿using System.Threading.Tasks;
using ExporterWebApi.Digizuite.core;

namespace ExporterWebApi.Interfaces
{
    internal interface ISearchForAssetsAndMetadata
    {
        IProvideConfiguration Configs { get; set; }
        Task<SearchResult> DoSearch(int page);
    }
}
