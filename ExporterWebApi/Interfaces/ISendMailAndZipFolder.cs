﻿using System.Net.Mail;
using ExporterWebApi.Exporter.Core.Model.Mediation;

namespace ExporterWebApi.Interfaces
{
    /// <summary>
    /// Defines a class, which can zip directories and send mails
    /// </summary>
    public interface ISendMailAndZipFolder
    {
        /// <summary>
        /// Sends a mail with the zipped file as an attachment
        /// </summary>
        /// <param name="mailAddress">The mail address of the recipient</param>
        /// <param name="downloadUrl"></param>
        /// <param name="folderPath"></param>
        /// <param name="errorMessage">Any errors to include in the mail</param>
        void SendMail(MailAddress mailAddress, string downloadUrl, string folderPath, string errorMessage);

        /// <summary>
        /// Zips the export and moves it to storage
        /// </summary>
        /// <param name="folderPath">The folder containing the export on localhost</param>
        void ZipAndMoveExportToStorage(string folderPath);

        /// <summary>
        /// Gets a zipped export's bytes from storage
        /// </summary>
        /// <param name="uniqueFileName"></param>
        /// <param name="deleteExport"></param>
        /// <returns></returns>
        byte[] GetZippedExportFromStorage(string uniqueFileName, bool deleteExport);
    }
}
