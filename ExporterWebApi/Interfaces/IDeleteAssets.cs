﻿using System.Collections.Generic;

namespace ExporterWebApi.Interfaces
{
    internal interface IDeleteAssets
    {
        void DeleteAssets(List<IAsset> assets);
    }
}
