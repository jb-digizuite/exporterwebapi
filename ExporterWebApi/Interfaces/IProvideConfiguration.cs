﻿using System;
using System.Collections.Generic;
using ExporterWebApi.Enums;
using ExporterWebApi.Exporter.WebApi.Controllers.RequestModel;
using ExporterWebApi.MessageHost.Messages.ExportRequest;

namespace ExporterWebApi.Interfaces
{
    /// <summary>
    /// Defines a ConfigurationProvider
    /// </summary>
    public interface IProvideConfiguration
    {
        /// <summary>
        /// The URL of the DAM Center API
        /// </summary>
        Uri DamApiUrl { get; }

        /// <summary>
        /// The search used to extract meta data and download information for assets
        /// </summary>
        string SearchName { get; }

        /// <summary>
        /// The different possibilities when transforming meta data
        /// </summary>
        MetadataTransformType MetadataTransformType { get; }

        /// <summary>
        /// The different possibilities when exporting assets
        /// </summary>
        AssetExportType AssetExportType { get; }

        /// <summary>
        /// The different possibilities when exporting meta data
        /// </summary>
        MetadataDeliverType MetadataDeliverType { get; }

        /// <summary>
        /// Determines whether assets are exported
        /// </summary>
        bool ExportAssets { get; set; }

        /// <summary>
        /// Determines whether meta data is exported
        /// </summary>
        bool ExportMetadata { get; set; }

        /// <summary>
        /// The path to the folder where the exports are placed on the local disc
        /// </summary>
        string FolderPath { get; set; }

        /// <summary>
        /// Determines whether the exported assets should be "soft deleted"
        /// </summary>
        bool DeleteExportedAssets { get; }

        /// <summary>
        /// The Search2 search XML ID of the Title valueField. Becomes a part of an asset's file name
        /// </summary>
        string TitleFieldId { get; }

        /// <summary>
        /// The Search2 search XML ID of the extension valueField. Becomes an asset's file extension
        /// </summary>
        string ExtensionFieldId { get; }

        /// <summary>
        /// The Search2 search XML ID of the URL used to download an asset's physical file
        /// </summary>
        string UrlFieldId { get; }

        /// <summary>
        /// Search2 search XML valueField IDs to exclude from the extracted meta data
        /// </summary>
        List<string> MetadataExludeFieldIds { get; }

        /// <summary>
        /// Some exported meta data are not simple types, i.e. "multi combo value". These types are joined into a string separated by this char
        /// </summary>
        string Splitter { get; }

        /// <summary>
        /// A search parameter. If this is supplied, only assets within the specified channel will be exported
        /// </summary>
        int ChannelId { get; }

        /// <summary>
        /// A search parameter. If this is supplied, only assets within the specified catalog folder will be exported
        /// </summary>
        int CatalogFolderId { get; }

        /// <summary>
        /// A search parameter. If this is supplied, only assets with the specified asset IDs will be exported
        /// </summary>
        List<int> AssetIds { get; set; }

        /// <summary>
        /// Part of the search paging information. Determines the maximum of assets returned by each search request
        /// </summary>
        int AssetsPerPage { get; }

        /// <summary>
        /// The Search2 search XML ID of the total amount of assets, which can be returned by the search. 
        /// </summary>
        string TotalFieldId { get; }

        /// <summary>
        /// The Search2 search XML ID of the asset type
        /// </summary>
        string AssetTypeFieldId { get; }

        /// <summary>
        /// Sets the MaxDegreeOfParallelism property when the assets are downloaded in parallel.
        /// </summary>
        int MaxDegreeOfParallelism { get; }

        /// <summary>
        /// The mail address used to inform that an export has completed and is ready for download
        /// </summary>
        string MailAdress { get; }

        /// <summary>
        /// Authenticates a user when the API retrieves a request. Also used to authorize the read access to the assets and meta data when executing the search
        /// </summary>
        string AccessKey { get; set; }

        /// <summary>
        /// Used to tell the clients where the export can be downloaded
        /// </summary>
        string DownloadExportUrl { get; set; }

        /// <summary>
        /// The name of the folder on the disc. Also becomes part of the zipped export's name.
        /// </summary>
        Guid UniqueFolderName { get; set; }

        /// <summary>
        /// Overwrites some configurations with settings, the client has decided upon
        /// </summary>
        /// <param name="request">An NServicebus message</param>
        void OverwriteConfigs(BaseRequest request);

        /// <summary>
        /// Overwrites some configurations with settings, the client has decided upon
        /// </summary>
        /// <param name="request">The base HTTP request</param>
        void OverwriteConfigs(IExportRequest request);
    }
}
