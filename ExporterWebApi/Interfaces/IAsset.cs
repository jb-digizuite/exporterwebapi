﻿using System.Collections.Generic;

namespace ExporterWebApi.Interfaces
{
    /// <summary>
    /// Defines a very simple asset
    /// </summary>
    public interface IAsset
    {
        /// <summary>
        /// The asset's meta data. 
        /// </summary>
        Dictionary<string, string> Metadata { get; }

        /// <summary>
        /// The asset's item ID
        /// </summary>
        int ItemId { get; }

        /// <summary>
        /// The asset's file name
        /// </summary>
        string FileName { get; }

        /// <summary>
        /// Downloads the asset to the folderPath on the local disc
        /// </summary>
        /// <param name="folderPath">Where the asset is saved</param>
        void DownloadAsssetToFolder(string folderPath);
    }
}
