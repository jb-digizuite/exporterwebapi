﻿using System.Collections.Generic;

namespace ExporterWebApi.MessageHost.Messages.ExportRequest
{
    public class ExportRequest : IExportRequest
    {
        public string MetadataTransformType { get; set; }
        public bool ExportAssets { get; set; }
        public bool ExportMetadata { get; set; }
        public bool DeleteExportedAssets { get; set; }
        public List<string> MetadataExludeFieldIds { get; set; }
        public string MailAddress { get; set; }
        public string AccessKey { get; set; }
        public List<int> AssetIds { get; set; }
        public int CatalogFolderId { get; set; }
        public int ChannelId { get; set; }
    }
}
