﻿using System.Collections.Generic;
using NServiceBus;

namespace ExporterWebApi.MessageHost.Messages.ExportRequest
{
    public interface IExportRequest : ICommand
    {
        string MetadataTransformType { get; set; }
        bool ExportAssets { get; set; }
        bool ExportMetadata { get; set; }
        bool DeleteExportedAssets { get; set; }
        List<string> MetadataExludeFieldIds { get; set; }
        string MailAddress { get; set; }
        string AccessKey { get; set; }
        List<int> AssetIds { get; set; }
        int CatalogFolderId { get; set; }
        int ChannelId { get; set; }
    }
}
